function cari() {
  var nim = $('#nim').val();
  var alamat = $('#alamat').val();
  var angkatan = $('#tahun_angkatan').val();
  load_filter(nim, alamat, angkatan);
}






function load_filter(nim, alamat, tahun_angkatan) { //meload map
  var map = new google.maps.Map(document.getElementById("map"), {
    center: new google.maps.LatLng(-0.2134415, 121.512737),
    zoom: 4, //tingkat zoom map, sesuaikan kebutuhan
    mapTypeId: 'roadmap'
  });
  var infoWindow = new google.maps.InfoWindow;

  downloadUrl("aksi_filter.php?nim=" + nim + "&alamat=" + alamat + "&tahun_angkatan=" + tahun_angkatan, function(data) {
    var xml = data.responseXML;
    var markers = xml.documentElement.getElementsByTagName("marker");
    for (var i = 0; i < markers.length; i++) {
      var name = markers[i].getAttribute("name");
      var address = markers[i].getAttribute("address");
      var type = markers[i].getAttribute("type");
      var point = new google.maps.LatLng(
        parseFloat(markers[i].getAttribute("lat")),
        parseFloat(markers[i].getAttribute("lng")));
      var html = "<b>" + name + "</b> <br/>" + address + "</b> <br/>Keterangan :" + type;
      var marker = new google.maps.Marker({
        map: map,
        position: point,
        icon: '../../assets/image/marker/marker1.png' //sesuakan nama gambar dan lokasi marker anda
      });
      bindInfoWindow(marker, map, infoWindow, html);
    }
  });
}



// Change this depending on the name of your PHP file


function bindInfoWindow(marker, map, infoWindow, html) {
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });
}

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ?
    new ActiveXObject('Microsoft.XMLHTTP') :
    new XMLHttpRequest;

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };
  request.open('GET', url, true);
  request.send(null);
}

function doNothing() {}
