     // ======================== Menampilkan Data di Database =============================


      function tabel_data(){
         $.get("data_biodata.php", {}, function (data, status) {
            $(".x_content").html(data);
        });
      }

    // ======================== Menampilkan Data di Database ============================= -->



   // ======================== Menampilkan MAPS  =============================


      function map(){
         $.get("maps.html", {}, function (data, status) {
            $(".maps").html(data);
        });
      }





    // ======================== Action Tombol Tambah =============================

       function tambah()
       {
          // save_method = 'add';
          $('#demo-form2')[0].reset(); 
          $('#form1').modal('show'); 
          $('.modal-title').text('Tambah');

          button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="simpan()">'+
                    'tambah'+
                  '</button>';
          $(".modal-footer").html(button);
        }

    // ======================== Action Tombol Tambah =============================

    // ======================== Simpan Data ============================= -->
        function simpan() {
          $.ajax({
            url: "aksi.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=simpan",
            success: function(res) {
              alert(res);
              if(res=="gagal"){
                alert("Coba Cek Kembali");
              }else{
                window.location.href = "biodata.php";
              }
              
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Simpan Data ============================= -->





    // ======================== Action TOmbol Edit=============================

       function edit(nim)
       {
          $('#form1').modal('show'); 
          $('.modal-title').text('Perbarui');
            button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="perbarui()">'+
                    'perbarui'+
                  '</button>';

          $.ajax({
          url: "aksi.php",
          type: "post",
          data: "proses=edit&id="+nim,
          dataType: "json",
          success: function(res) {
            $('[name="nim"]').val(res.nim);
            $('[name="nama_lengkap"]').val(res.nama_lengkap);
            $('[name="alamat"]').val(res.alamat);
            $('[name="no_hp"]').val(res.no_hp);
            $('[name="ketua_angkatan"]').val(res.ketua_angkatan);
            $(".modal-footer").html(button);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });

        }

    // ======================== Action TOmbol Edit=============================





   
    // ======================== Perbarui Data ============================= -->
        function perbarui() {
          $.ajax({
            url: "aksi.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=perbarui",
            success: function(res) {
              if(res=="gagal"){
                alert("Coba Cek Kembali");
              }else{
                window.location.href = "biodata.php";
              }

            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Perbarui Data ============================= -->










    // ======================== Hapus Data ============================= -->
         function hapus(nim) {
          var conf = confirm("Apakah ingin di hapus?");
          if (conf == true) {
              $.ajax({
                url: "aksi.php",
                type: "post",
                data: "proses=hapus&id="+nim,
                success: function(res) {
                  tabel_data();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
                }
              });
          }
        }
    // ======================== Hapus Data ============================= -->

