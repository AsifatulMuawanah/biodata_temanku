            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Ketua Angkatan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>

                <tbody >
                  <?php
                    include_once '../../koneksi/koneksi.php';
                    $query="select * from data_diri ORDER BY `data_diri`.`nim` ASC";
                    $eksekusi=mysql_query($query);
                    $no=1;
                    while($data=mysql_fetch_array($eksekusi))
                    { ?>
                      <tr>
                      <td>
                        <?php echo $no; ?>
                      </td>
                      <td>
                        <?php echo $data['nim']; ?>
                      </td>
                      <td>
                        <?php echo $data['nama_lengkap']; ?>
                      </td>
                      <td>
                        <?php echo $data['alamat']; ?>
                      </td>
                      <td>
                        <?php echo $data['no_hp']; ?>
                      </td>
                      <td>
                        <?php echo $data['ketua_angkatan']; ?>
                      </td>

                      <td>
                          <a href="form_biodata.php?nim=<?=$data['nim'];?>&proses=update"><button type="button" class="btn btn-round bg-green">Detail</button></a>

                      </td>
                    </tr>
                    <?php
                      $no++;}
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Ketua Angkatan</th>
                    <th>Aksi</th>
                    </tr>
                </tfoot>
              </table>

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({
      keys: true
    });
    $('#datatable-responsive').DataTable();
    $('#datatable-scroller').DataTable({
      ajax: "../../assets/gentelella/production/js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
    var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });
  });
  TableManageButtons.init();
</script>
