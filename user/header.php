<?php
  include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

  <title> User </title>

  <!-- Bootstrap core CSS -->

  <link href="../../assets/gentelella/production/css/bootstrap.min.css" rel="stylesheet">

  <link href="../../assets/gentelella/production/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../assets/gentelella/production/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="../../assets/gentelella/production/css/custom.css" rel="stylesheet">
  <!-- <link rel="stylesheet" type="text/css" href="../../assets/gentelella/production/css/maps/jquery-jvectormap-2.0.3.css" /> -->
  <link href="../../assets/gentelella/production/css/icheck/flat/green.css" rel="stylesheet" />
  <link href="../../assets/gentelella/production/css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="../../assets/gentelella/production/js/jquery.min.js"></script>
  <script src="../../assets/gentelella/production/js/nprogress.js"></script>

  <script src="../../assets/gentelella/production/js/jquery-1.11.3.js"></script>


  <link href="../../assets/gentelella/production/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/gentelella/production/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/gentelella/production/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/gentelella/production/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/gentelella/production/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

  <script src="../../assets/gentelella/production/js/jquery.min.js"></script>
  <script src="../../assets/Chart/Chart.bundle.js"></script>
  <style type="text/css">
      /*.container {
          width: 40%;
          margin: 15px auto;
      }*/
  </style>
  <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height            : 100%;
        margin            : 0;
        padding           : 0;
      }
      .controls {
        margin-top        : 10px;
        border            : 1px solid transparent;
        border-radius     : 2px 0 0 2px;
        box-sizing        : border-box;
        -moz-box-sizing   : border-box;
        height            : 32px;
        outline           : none;
        box-shadow        : 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color  : #fff;
        font-family       : Roboto;
        font-size         : 15px;
        font-weight       : 300;
        margin-left       : 12px;
        padding           : 0 11px 0 13px;
        text-overflow     : ellipsis;
        width             : 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
    </style>

<script type="text/javascript" src="../../assets/gentelella/production/js/moment/moment.min.js"></script>
<script type="text/javascript" src="../../assets/gentelella/production/js/datepicker/daterangepicker.js"></script>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
  function load() { //meload map
    var map = new google.maps.Map(document.getElementById("map"), {
      center: new google.maps.LatLng(-0.2134415,121.512737),
      zoom: 4, //tingkat zoom map, sesuaikan kebutuhan
      mapTypeId: 'roadmap'
    });
    var infoWindow = new google.maps.InfoWindow;

      downloadUrl("aksi_all.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var type = markers[i].getAttribute("type");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html = "<b>" + name + "</b> <br/>" + address+ "</b> <br/>Keterangan :" +type;
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: '../../assets/image/marker/marker1.png' //sesuakan nama gambar dan lokasi marker anda
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
      }



    // Change this depending on the name of your PHP file
  function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(html);
      infoWindow.open(map, marker);
    });
  }
  function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function() {
      if (request.readyState == 4) {
        request.onreadystatechange = doNothing;
        callback(request, request.status);
      }
    };
    request.open('GET', url, true);
    request.send(null);
  }
  function doNothing() {}
</script>
</head>


<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"><i class="fa fa-users"></i> <span>BIODATA TEMAN TI</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="../../assets/gentelella/production/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Selamat Datang,</span>
              <h2><?php echo $nama; ?></h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />
          <br>
          <br>
          <br>

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">

              <ul class="nav side-menu">
                <li><a href="../dashboard/dashboard.php"><i class="fa fa-home"></i> Dashboard</a>
                </li>

                <li><a href="../biodata/biodata.php"><i class="fa fa-user"></i> Biodata Teman </a>
                </li>


                <li><a href="../forum/forum.php"><i class="fa fa-user"></i> Forum</a>
                </li>
                </li>
                  <li><a href="../Chart/c.php"><i class="fa fa-bar-chart"></i> Chart </a>
                </li>



              </ul>
            </div>


          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="../../assets/gentelella/production/images/img.jpg" alt=""><?php echo $nama; ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="javascript:;">  Profile</a>
                  </li>
                  <li><a href="../logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>

              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                  <li>
                    <a>
                      <span class="image">
                                        <img src="../../assets/gentelella/production/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="../../assets/gentelella/production/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="../../assets/gentelella/production/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="../../assets/gentelella/production/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <div class="text-center">
                      <a href="inbox.html">
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->
