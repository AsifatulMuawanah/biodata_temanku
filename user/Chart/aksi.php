<?php
	include_once '../../koneksi/koneksi.php';
	
	if(isset($_POST['proses']) and $_POST['proses']=='filter_matkul'){
		filter_matkul();
	}

	
	function filter_matkul()
	{	
		$koneksi      = mysqli_connect("localhost", "root", "", "biodata_teman_ti");

		
	    $matkul_nama  = mysqli_query($koneksi,"SELECT `data_diri_matkul`.`id_matkul`,COUNT(*), `matkul`.`nama_matkul` from `data_diri_matkul`, `matkul` where `matkul`.`id_matkul` = `data_diri_matkul`.`id_matkul` GROUP BY `matkul`.`id_matkul` ");
	     $matkul_count= mysqli_query($koneksi,"SELECT `data_diri_matkul`.`id_matkul`,COUNT(*), `matkul`.`nama_matkul` from `data_diri_matkul`, `matkul` where `matkul`.`id_matkul` = `data_diri_matkul`.`id_matkul` GROUP BY `matkul`.`id_matkul` ");

		$matkul 		= $_POST['matkul'];
		$res[]="";
		for($d=0;$d<count('matkul');$d++){
			$res[$d] = $matkul[$d];
		}

		$chart;

		$chart='<script>'+
			    'var ctx = document.getElementById("chart_matkul");'+
			    'var chart_matkul = new Chart(ctx, {'+
			        'type: "bar",'+
			        'data: {'+
			            'labels: ['+
			                      '<?php '+
			                          '// $no=0;'+
			                          'while ($m = mysqli_fetch_array('+'$matkul_nama)) {'+
			                                 'if($m["id_matkul"]==$res[$no]){'+
			                                     'echo ".$m["nama_matkul"].",";'+
			                                  '}'+
			                          '}'+
			                      '?>'+
			                    '],'+
			            'datasets: [{'+
			                    'label: "# of Votes",'+
			                    'data: ['+
			                            '<?php '+
			                                '// $coba=2;'+
			                                'while ($p = mysqli_fetch_array($matkul_count)) { '+
			                                  'if($m["id_matkul"]==$res[$no]){'+
			                                   		'echo ".$p["COUNT(*)"].",";'+
			                                  ' }'+
			                                '}'+
			                            '?>'+
			                          '],'+
			                    'backgroundColor: ['+
			                        '"rgba(255, 99, 132, 0.2)",'+
			                        '"rgba(54, 162, 235, 0.2)",'+
			                        '"rgba(255, 206, 86, 0.2)",'+
			                        '"rgba(75, 192, 192, 0.2)",'+
			                        '"rgba(153, 102, 255, 0.2)",'+
			                        '"rgba(255, 159, 64, 0.2)",'+
			                        '"rgba(255, 99, 132, 0.2)",'+
			                        '"rgba(54, 162, 235, 0.2)",'+
			                        '"rgba(255, 206, 86, 0.2)",'+
			                        '"rgba(75, 192, 192, 0.2)",'+
			                        '"rgba(153, 102, 255, 0.2)",'+
			                        '"rgba(255, 159, 64, 0.2)"'+
			                    '],'+
			                    'borderColor: ['+
			                        '"rgba(255,99,132,1)",'+
			                        '"rgba(54, 162, 235, 1)",'+
			                        '"rgba(255, 206, 86, 1)",'+
			                        '"rgba(75, 192, 192, 1)",'+
			                        '"rgba(153, 102, 255, 1)",'+
			                        '"rgba(255, 159, 64, 1)",'+
			                        '"rgba(255, 99, 132, 0.2)",'+
			                        '"rgba(54, 162, 235, 0.2)",'+
			                        '"rgba(255, 206, 86, 0.2)",'+
			                        '"rgba(75, 192, 192, 0.2)",'+
			                        '"rgba(153, 102, 255, 0.2)",'+
			                        '"rgba(255, 159, 64, 0.2)"'+
			                    '],'+
			                    'borderWidth: 1'+
			                '}]'+
			        '},'+
			        'options: {'+
			            'scales: {'+
			                'yAxes: [{'+
			                        'ticks: {'+
			                            'beginAtZero: true'+
			                        '}'+
			                    '}]'+
			            '}'+
			        '}'+
			    '});'+
			'</script>';

		// echo $res;
		echo json_encode($res);
	}

 ?>
