-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19 Jun 2017 pada 12.23
-- Versi Server: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biodata_teman_ti`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `angkatan`
--

CREATE TABLE `angkatan` (
  `id_angkatan` int(100) NOT NULL,
  `tahun_angkatan` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `angkatan`
--

INSERT INTO `angkatan` (`id_angkatan`, `tahun_angkatan`) VALUES
(1, 2001),
(3, 2014),
(4, 2015),
(5, 2016),
(6, 2017),
(10, 2013),
(11, 2018);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_diri`
--

CREATE TABLE `data_diri` (
  `nim` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lang` float(10,6) NOT NULL,
  `rt` varchar(100) NOT NULL,
  `rw` varchar(100) NOT NULL,
  `no_rumah` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `id_angkatan` int(100) NOT NULL,
  `cita_cita` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ketua_angkatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_diri`
--

INSERT INTO `data_diri` (`nim`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `lat`, `lang`, `rt`, `rw`, `no_rumah`, `email`, `no_hp`, `id_angkatan`, `cita_cita`, `password`, `ketua_angkatan`) VALUES
('14650024', 'Denny Setia Wisnnugraha', 'L', 'Magetan', '05/01/2017', 'Jl. Desa Keniten, Keniten, Geneng, Kabupaten Ngawi, Jawa Timur 63271, Indonesia', -7.514981, 111.442566, '', '', '', 'denny@gmail.com', '9034859', 0, 'Programmer', '1', 'Tidak'),
('14650030', 'Maulidah Fitriyah', 'P', 'gresik', '05/30/2017', 'Jl. Raya Gresik, Kemantren, Paciran, Kabupaten Lamongan, Jawa Timur 62264, Indonesia', -6.876909, 112.412773, '90', '90', '90', 'fitri@gmail', '29348', 0, 'admin', '1', 'Tidak'),
('14650036', 'Dinda Ockta N', 'P', 'Malang', '22/10/2017', 'Jl. Kolonel Sugiono, Brontokusuman, Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta, Indonesia', -7.815427, 110.373688, '6', '6', '5', 'dindaocktan@gmail.com', '08967584612', 0, 'Desainer', '1', 'Iya'),
('14650045', 'Lia Khoirunissa', 'L', 'Tranggalek', '08/25/1995', 'Sukorejo, Tugu, Trenggalek Regency, East Java, Indonesia', -8.041135, 111.643715, '26', '13', '', 'lia@gmail.com', '08167548967', 3, 'Desainer', '1', 'Tidak'),
('14650060', 'Galang Luhur Pekerti', 'L', 'Ponorogo', '12/08/1994', 'Jalan Sekar Harum, Tonatan, Kec. Ponorogo, Kabupaten Ponorogo, Jawa Timur, Indonesia', -7.869540, 111.485344, '1', '3', '', 'galang@gmail.com', '08947827364231', 3, 'Apa saja', '1', 'Tidak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_diri_komunitas`
--

CREATE TABLE `data_diri_komunitas` (
  `id_ddk` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `id_komunitas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_diri_komunitas`
--

INSERT INTO `data_diri_komunitas` (`id_ddk`, `nim`, `id_komunitas`) VALUES
(32, 14650060, 1),
(33, 14650045, 4),
(34, 14650024, 1),
(35, 14650024, 2),
(36, 14650030, 3),
(37, 14650030, 5),
(40, 14650036, 1),
(41, 14650036, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_diri_matkul`
--

CREATE TABLE `data_diri_matkul` (
  `id_ddm` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_diri_matkul`
--

INSERT INTO `data_diri_matkul` (`id_ddm`, `nim`, `id_matkul`) VALUES
(42, 14650060, 4),
(43, 14650045, 6),
(44, 14650024, 3),
(45, 14650024, 5),
(46, 14650024, 8),
(47, 14650024, 12),
(48, 14650030, 3),
(49, 14650030, 7),
(50, 14650030, 9),
(54, 14650036, 3),
(55, 14650036, 5),
(56, 14650036, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_diri_skill`
--

CREATE TABLE `data_diri_skill` (
  `id_dds` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `id_skill` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_diri_skill`
--

INSERT INTO `data_diri_skill` (`id_dds`, `nim`, `id_skill`) VALUES
(30, 14650060, 4),
(31, 14650045, 4),
(32, 14650045, 9),
(33, 14650024, 3),
(34, 14650024, 4),
(35, 14650030, 5),
(36, 14650030, 9),
(37, 14650030, 10),
(41, 14650036, 3),
(42, 14650036, 5),
(43, 14650036, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `id_reply` int(100) NOT NULL,
  `id_topik` int(100) NOT NULL,
  `nim` int(100) NOT NULL,
  `isi_komen` varchar(300) NOT NULL,
  `tgl_komen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`id_reply`, `id_topik`, `nim`, `isi_komen`, `tgl_komen`) VALUES
(1, 5, 14650001, 'apa yaa kok bingung', '24-05-2017\r\n'),
(2, 4, 14650024, 'apa siih ini', '29/05/2017/ 07:05:52'),
(12, 4, 14650024, 'HJH', '29/05/2017/ 08:05:43'),
(13, 4, 14650024, 'HJH', '29/05/2017/ 08:05:43'),
(14, 4, 14650024, 'HJH', '29/05/2017/ 08:05:43'),
(15, 4, 14650001, 'apasiih gak jelAS', '29/05/2017/ 08:05:12'),
(16, 4, 14650001, 'jjj', '30/05/2017/ 08:05:25'),
(17, 4, 14650001, 'apa sih loo', '30/05/2017/ 08:05:25'),
(18, 4, 14650001, 'iyaaa..apa :)', '30/05/2017/ 08:05:06'),
(19, 4, 14650001, 'fdjsdf', '30/05/2017/ 08:05:57'),
(20, 4, 14650036, 'gak ada komen sih menurutku', '30/05/2017/ 08:05:10'),
(21, 4, 14650036, 'masa', '30/05/2017/ 08:05:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komunitas`
--

CREATE TABLE `komunitas` (
  `id_komunitas` int(100) NOT NULL,
  `nama_komunitas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komunitas`
--

INSERT INTO `komunitas` (`id_komunitas`, `nama_komunitas`) VALUES
(1, 'ETH0'),
(2, 'UINbuntu'),
(3, 'Multi Media'),
(4, 'ONTAKI'),
(5, 'Weboender');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkul`
--

CREATE TABLE `matkul` (
  `id_matkul` int(11) NOT NULL,
  `nama_matkul` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `matkul`
--

INSERT INTO `matkul` (`id_matkul`, `nama_matkul`) VALUES
(3, 'IMK (Interaksi Manusia dan Komputer)'),
(4, 'PTI (Pengantar Teknologi Informasi)'),
(5, 'Pemerogram Web'),
(6, 'Software Quality'),
(7, 'Arsitektur Enterprise'),
(8, 'Jaringan Komputer'),
(9, 'Grafika Komputer'),
(10, 'Elektronika Digital'),
(11, 'Multimedia dan Game'),
(12, 'Sistem Operasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `skill`
--

CREATE TABLE `skill` (
  `id_skill` int(100) NOT NULL,
  `nama_skill` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `skill`
--

INSERT INTO `skill` (`id_skill`, `nama_skill`) VALUES
(3, 'Front End'),
(4, 'Back End'),
(5, 'Analisis'),
(6, 'Quality Control (QC)'),
(7, 'Quality Assurance (QA)'),
(8, 'Project Manager'),
(9, 'Desain 2D'),
(10, 'Desain 3D'),
(11, 'Membuat Game'),
(12, 'Merangkai Jaringan'),
(13, 'Animator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `topik`
--

CREATE TABLE `topik` (
  `id_topik` int(100) NOT NULL,
  `id_matkul` int(100) NOT NULL,
  `nim` int(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `tgl_post` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `topik`
--

INSERT INTO `topik` (`id_topik`, `id_matkul`, `nim`, `judul`, `deskripsi`, `tgl_post`) VALUES
(4, 3, 14650024, 'belajar IMK', 'Pengertian IMK mungkin ada yang taudsj', '04/06/2017/ 04:06:33'),
(10, 4, 14650001, 'IMK part 6', 'Pengertian IMK mungkin ada yang tau', '22/05/2017/ 03:05:44'),
(12, 4, 14650001, 'PTI part I', 'Belajar PTI itu menyenangkan looo', '25/05/2017/ 03:05:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angkatan`
--
ALTER TABLE `angkatan`
  ADD PRIMARY KEY (`id_angkatan`);

--
-- Indexes for table `data_diri`
--
ALTER TABLE `data_diri`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `data_diri_komunitas`
--
ALTER TABLE `data_diri_komunitas`
  ADD PRIMARY KEY (`id_ddk`),
  ADD KEY `nim` (`nim`),
  ADD KEY `id_komunitas` (`id_komunitas`);

--
-- Indexes for table `data_diri_matkul`
--
ALTER TABLE `data_diri_matkul`
  ADD PRIMARY KEY (`id_ddm`),
  ADD KEY `nim` (`nim`),
  ADD KEY `id_matkul` (`id_matkul`);

--
-- Indexes for table `data_diri_skill`
--
ALTER TABLE `data_diri_skill`
  ADD PRIMARY KEY (`id_dds`),
  ADD KEY `nim` (`nim`),
  ADD KEY `id_skill` (`id_skill`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_reply`);

--
-- Indexes for table `komunitas`
--
ALTER TABLE `komunitas`
  ADD PRIMARY KEY (`id_komunitas`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`id_matkul`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id_skill`);

--
-- Indexes for table `topik`
--
ALTER TABLE `topik`
  ADD PRIMARY KEY (`id_topik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angkatan`
--
ALTER TABLE `angkatan`
  MODIFY `id_angkatan` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `data_diri_komunitas`
--
ALTER TABLE `data_diri_komunitas`
  MODIFY `id_ddk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `data_diri_matkul`
--
ALTER TABLE `data_diri_matkul`
  MODIFY `id_ddm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `data_diri_skill`
--
ALTER TABLE `data_diri_skill`
  MODIFY `id_dds` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_reply` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `komunitas`
--
ALTER TABLE `komunitas`
  MODIFY `id_komunitas` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `matkul`
--
ALTER TABLE `matkul`
  MODIFY `id_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `id_skill` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `topik`
--
ALTER TABLE `topik`
  MODIFY `id_topik` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
