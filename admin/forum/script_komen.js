     // ======================== Menampilkan Data di Database =============================


      function tampil_komen(id){
           $.ajax({
            url  : "komentar.php",
            type : "post",
            data : "id="+id,
            
            success: function(res) {
               $("#komentar").html(res);              
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
      }

    // ======================== Menampilkan Data di Database ============================= -->






    // ======================== Action Tombol Tambah =============================

       function tambah()
       {
          // save_method = 'add';
          $('#demo-form2')[0].reset(); 
          $('#form1').modal('show'); 
          $('.modal-title').text('Tambah');

          button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="simpan()">'+
                    'tambah'+
                  '</button>';
          $(".modal-footer").html(button);
        }

    // ======================== Action Tombol Tambah =============================

    // ======================== Simpan Data ============================= -->
        function simpan(id) {
          $.ajax({
            url: "aksi_komen.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=simpan",
            success: function(res) {
              tampil_komen(id);
              
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Simpan Data ============================= -->





    // ======================== Action TOmbol Edit=============================

       function edit(id_topik)
       {
          $('#form1').modal('show'); 
          $('.modal-title').text('Perbarui');
            button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="perbarui()">'+
                    'perbarui'+
                  '</button>';

          $.ajax({
          url: "aksi_komen.php",
          type: "post",
          data: "proses=edit&id="+id_topik,
          dataType: "json",
          success: function(res) {
            $('[name="id_reply"]').val(res.id_reply);
            $('[name="id_matkul"]').val(res.id_matkul);
            $('[name="nim"]').val(res.nim);
            $('[name="judul"]').val(res.judul);
            $('[name="deskripsi"]').val(res.deskripsi);
            $(".modal-footer").html(button);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });

        }

    // ======================== Action TOmbol Edit=============================





   
    // ======================== Perbarui Data ============================= -->
        function perbarui() {
          $.ajax({
            url: "aksi.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=perbarui",
            success: function(res) {
        
            tabel_data();

            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Perbarui Data ============================= -->










    // ======================== Hapus Data ============================= -->
         function hapus(id_topik) {
          var conf = confirm("Apakah ingin di hapus?");
          if (conf == true) {
              $.ajax({
                url: "aksi.php",
                type: "post",
                data: "proses=hapus&id="+id_topik,
                success: function(res) {
                  tabel_data();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
                }
              });
          }
        }
    // ======================== Hapus Data ============================= -->
