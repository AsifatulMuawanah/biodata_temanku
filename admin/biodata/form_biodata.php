<?php
    include "../header.php";
    include '../../koneksi/koneksi.php';
    $nim            = "";
    $nama_lengkap   = "";
    $jenis_kelamin  = "";
    $tempat_lahir   = "";
    $tanggal_lahir  = "";
    $alamat         = "";
    $lat            = "";
    $leng           = "";
    $rt             = "";
    $rw             = "";
    $no_rumah       = "";
    $email          = "";
    $no_hp          = "";
    $tahun_angkatan = "";
    $cita2          = "";
    $password       = "";
    $ketua_angkatan = "";
    $matkul[]       = "";
    $komunitas[]    = "";
    $skill[]        = "";


    $proses   = $_GET['proses'];
    if($proses == 'update') {
      $nim     = $_GET['nim'];
      $query   = "select * from data_diri where nim='$nim' ";
      $eksekusi= mysql_query($query);
      if($data = mysql_fetch_array($eksekusi)) {
          $nim            = $data['nim'];
          $nama_lengkap   = $data['nama_lengkap'];
          $jenis_kelamin  = $data['jenis_kelamin'] ;
          $tempat_lahir   = $data['tempat_lahir'];
          $tanggal_lahir  = $data['tanggal_lahir'] ;
          $alamat         = $data['alamat'] ;
          $lat            = $data['lat'] ;
          $leng           = $data['lang'] ;
          $rt             = $data['rt'];
          $rw             = $data['rw'];
          $no_rumah       = $data['no_rumah'];
          $email          = $data['email'] ;
          $no_hp          = $data['no_hp'];
          $tahun_angkatan = $data['id_angkatan'];
          $cita2          = $data['cita_cita'] ;
          $password       = $data['password'];
          $ketua_angkatan = $data['ketua_angkatan'] ;

            // $query_angkatan   = "select * from angkatan where id_angkatan='$id_angkatan' ";
            // $eksekusi_angkatan= mysql_query($query_angkatan);
            // if($data_angkatan = mysql_fetch_array($eksekusi_angkatan)) {
            //   $tahun_angkatan = $data_angkatan['tahun_angkatan'];
            // }

            $query_matkul     = "select * from data_diri_matkul where nim='$nim'";
            $eksekusi_matkul  = mysql_query($query_matkul);
            $no1=0;
            while($data_matkul= mysql_fetch_array($eksekusi_matkul)) {
              $matkul[$no1]    = $data_matkul['id_matkul'];
              $no1++;
            }

            $query_komunitas    = "select * from data_diri_komunitas where nim='$nim' ";
            $eksekusi_komunitas = mysql_query($query_komunitas);
            $no2=0;
            while($data_komunitas  = mysql_fetch_array($eksekusi_komunitas)) {
              $komunitas[$no2]  = $data_komunitas['id_komunitas'];
              $no2++;
            }

            $query_skill        = "select * from data_diri_skill where nim='$nim' ";
            $eksekusi_skill     = mysql_query($query_skill);
            $no2=0;
            while($data_skill   = mysql_fetch_array($eksekusi_skill)) {
              $skill[$no2]      = $data_skill['id_skill'];
              $no2++;
            }
      }
    }
?>

 <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Form Biodata Teman</h3>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>


        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="javascript:;">
                 <div class="item form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">NIM
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="nim" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nim"  type="text" value="<?=$nim;?>">
                      </div>
                    </div>

                   <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Lengkap
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nama_lengkap"   type="text" value="<?=$nama_lengkap;?>">
                      </div>
                    </div>


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="email" name="email"  class="form-control col-md-7 col-xs-12" value="<?=$email;?>">
                      </div>
                    </div>

                     <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_kelamin">Jenis Kelamin
                     </label>
                    <p>
                      <?php if($jenis_kelamin=="P"){
                             echo "&nbsp; &nbsp;";
                             echo '<input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" /> Laki-Laki';
                             echo "&nbsp; &nbsp; &nbsp;";
                             echo '<input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P" checked=" required"/> Perempuan';
                            }
                            else if($jenis_kelamin=="L"){
                              echo "&nbsp; &nbsp;";
                              echo '<input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" checked="required" /> Laki-Laki';
                              echo "&nbsp; &nbsp; &nbsp;";
                              echo 'P :<input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P"/> Perempuan';
                            }
                             else{
                              echo "&nbsp; &nbsp;";
                              echo '<input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L"/> Laki-Laki';
                              echo "&nbsp; &nbsp; &nbsp;";
                              echo 'P :<input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P"/> Perempuan';
                            }
                      ?>


                    </p>
                    </div>

                      <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tempat Lahir
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="nim" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="tempat_lahir"  type="text" value="<?=$tempat_lahir;?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="birthday" class="date-picker form-control col-md-7 col-xs-12"  type="text" name="tanggal_lahir" value="<?=$tanggal_lahir;?>">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Alamat
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="alamat"  name="alamat" class="form-control col-md-7 col-xs-12"><?=$alamat;?></textarea>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">
                        Alamat
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="pac-input" class="controls" type="text" placeholder="Cari Alamat" name="map">
                      </div>

                      <br><br>

                      <div class="col-sm-12" id="map" style="width: 600px; height: 400px; margin-left: 250px; ">
                        <?php
                                include 'maps.php';
                        ?>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">latidute
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="lat" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="lat"  type="text" value="<?=$lat;?>" readonly>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">longitude
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="leng" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="leng"  type="text" value="<?=$leng;?>" readonly>
                      </div>
                    </div>


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">RT
                      </label>
                      <div class="col-md-1 col-sm-2 col-xs-12">
                        <input id="rt" class="form-control col-md-6 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="rt" type="text" value="<?=$rt;?>">
                      </div>

                      <label class="control-label col-md-1 col-sm-2 col-xs-12" for="name">RW
                      </label>
                      <div class="col-md-1 col-sm-2 col-xs-12">
                        <input id="rw" class="form-control col-md-6 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="rw" type="text" value="<?=$rw;?>">
                      </div>


                      <label class="control-label col-md-1 col-sm-2 col-xs-12" for="name">No
                      </label>
                      <div class="col-md-1 col-sm-2 col-xs-12">
                        <input id="no_rumah" class="form-control col-md-6 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="no_rumah" type="text" value="<?=$no_rumah;?>">
                      </div>
                    </div>


                   <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telepon
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="tel" id="telephone" name="no_hp"  data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" value="<?=$no_hp;?>">
                      </div>
                    </div>


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="skill[]">Mata Kuliah Favorit
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php
                          $query="Select * from matkul";
                          $hasil=mysql_query($query);
                          $no=0;
                          $jumlah_matkul = count($matkul);

                          while($data=mysql_fetch_array($hasil)){
                            if($no!=$jumlah_matkul){
                              if($data['id_matkul']==$matkul[$no]){
                                echo "<input type='checkbox' class='flat' checked='checked' name='matkul[]' value='".$data['id_matkul']."'/>&nbsp;".$data['nama_matkul']."<br/>";
                                $no++;
                              }else{
                                echo "<input type='checkbox' class='flat'  name='matkul[]' value='".$data['id_matkul']."'/>&nbsp;".$data['nama_matkul']."<br/>";
                              }
                            }elseif($no==$jumlah_matkul){
                               echo "<input type='checkbox' class='flat'  name='matkul[]' value='".$data['id_matkul']."'/>&nbsp;".$data['nama_matkul']."<br/>";
                            }
                          }

                        ?>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="skill[]">Skill IT
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php
                          $query="Select * from skill";
                          $hasil=mysql_query($query);
                          $no=0;
                          $jumlah_skill = count($skill);

                          while($data=mysql_fetch_array($hasil)){
                            if($no!=$jumlah_skill){
                              if($data['id_skill']==$skill[$no]){
                                echo "<input type='checkbox' class='flat' checked='checked' name='skill[]' value='".$data['id_skill']."'/>&nbsp;".$data['nama_skill']."<br/>";
                                $no++;
                              }else{
                                echo "<input type='checkbox' class='flat'  name='skill[]' value='".$data['id_skill']."'/>&nbsp;".$data['nama_skill']."<br/>";
                              }
                            }elseif($no==$jumlah_skill){
                               echo "<input type='checkbox' class='flat'  name='skill[]' value='".$data['id_skill']."'/>&nbsp;".$data['nama_skill']."<br/>";
                            }
                          }

                        ?>
                      </div>
                    </div>&nbsp;


                     <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="komunitas[]">Komunitas
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php
                          $query="Select * from komunitas";
                          $hasil=mysql_query($query);
                          $no=0;
                          $jumlah_komunitas = count($komunitas);

                          while($data=mysql_fetch_array($hasil)){
                            if($no!=$jumlah_komunitas){
                              if($data['id_komunitas']==$komunitas[$no]){
                                echo "<input type='checkbox' class='flat' checked='checked' name='komunitas[]' value='".$data['id_komunitas']."'/>&nbsp;".$data['nama_komunitas']."<br/>";
                                $no++;
                              }else{
                                echo "<input type='checkbox' class='flat'  name='komunitas[]' value='".$data['id_komunitas']."'/>&nbsp;".$data['nama_komunitas']."<br/>";
                              }
                            }elseif($no==$jumlah_komunitas){
                               echo "<input type='checkbox' class='flat'  name='komunitas[]' value='".$data['id_komunitas']."'/>&nbsp;".$data['nama_komunitas']."<br/>";
                            }
                          }

                        ?>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Angkatan
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">

                       <select class="form-control" name="tahun_angkatan">
                        <option>-</option>
                        <?php
                          $query = "SELECT * FROM angkatan ORDER BY `angkatan`.`tahun_angkatan` ASC";
                          $hasil = mysql_query($query);
                          while ($data = mysql_fetch_array($hasil)){
                            if($data['id_angkatan']==$tahun_angkatan){
                               echo "<option value=".$data['id_angkatan']." selected='selected'>".$data['tahun_angkatan']."</option>";
                            }else{
                              echo "<option value='".$data['id_angkatan']."'>".$data['tahun_angkatan']."</option>";
                            }
                          }
                        ?>
                        </select>
                      </div>
                    </div>

                     <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cita-Cita
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="leng" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="cita2"  type="text" value="<?=$cita2;?>">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label for="password" class="control-label col-md-3">Password</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12"  value="<?=$password;?>">
                      </div>
                    </div>
                   <!--  <div class="item form-group">
                      <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="password2" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" >
                      </div>
                    </div> -->

                     <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ketua Angkatan
                      </label>
                      <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="ketua_angkatan">
                          <option>-</option> -->
                      <p>
                          <?php
                              if($ketua_angkatan=="Tidak"){
                                echo "&nbsp; &nbsp;";
                                echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Iya"/> Iya';
                                echo "&nbsp; &nbsp; &nbsp;";
                                echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Tidak" checked="required"/> Tidak';
                            }
                            else if($ketua_angkatan=="Iya"){
                                echo "&nbsp; &nbsp;";
                                echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Iya" checked="required"/> Iya';
                                echo "&nbsp; &nbsp; &nbsp;";
                                echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Tidak" /> Tidak';
                            }
                            else{
                              echo "&nbsp; &nbsp;";
                              echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Iya"/> Iya';
                              echo "&nbsp; &nbsp; &nbsp;";
                              echo '<input type="radio" class="flat" name="ketua_angkatan"  value="Tidak" /> Tidak';
                            }
                          ?>
                          </p>
                          <!-- <option value="Iya">Iya</option> -->
                         <!--  <option value="Tidak">Tidak</option>
                        </select>
                      </div> -->
                    </div>

                   <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <?php
                        if($proses=="update"){
                          echo '<button type="submit" class="btn btn-primary">Cancel</button>';
                          echo '<button type="submit" class="btn btn-success" onclick="perbarui()">Perbarui</button>';

                        }else{
                          echo '<button type="submit" class="btn btn-primary">Cancel</button>';
                          echo '<button type="submit" class="btn btn-success" onclick="simpan()">Simpan</button>';
                        }
                      ?>

                      </div>
                    </div>
                    <div class="ln_solid"></div>



                </form>
  <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right"> Praktikum WEB 2017  - Asifatul Mu'awanah & Dinda Ockta N
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->
                <?php
      include('../footer.php');
?>




<script type="text/javascript" src="script.js"> </script>
