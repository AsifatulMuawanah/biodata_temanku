<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Google Maps AJAX + mySQL/PHP Example</title>
    <script src="http://maps.google.com/maps/api/js?sensor=false"
            type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[

    function load() { //meload map
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(-0.2134415,121.512737),
        zoom: 4, //tingkat zoom map, sesuaikan kebutuhan
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("aksi_map_edit.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var type = markers[i].getAttribute("type");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html = "<b>" + name + "</b> <br/>" + address+ "</b> <br/>Keterangan :" +type;
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: '../../assets/image/marker/marker1.png' //sesuakan nama gambar dan lokasi marker anda
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };
      request.open('GET', url, true);
      request.send(null);
    }
    function doNothing() {}

    //]]>
  </script>
   <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9JKnBzOt5SoE2B4Z8C91SVg-izOEg7Tg&libraries=places&callback=initMap"
         async defer></script> -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9JKnBzOt5SoE2B4Z8C91SVg-izOEg7Tg&callback=initMap">
    </script>
  </head>

  <body onLoad="load()">
    <div id="map" style="width: 650px; height: 500px"></div>
  </body>
</html>