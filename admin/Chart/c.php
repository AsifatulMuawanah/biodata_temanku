<?php
  include "../header.php";
  include '../../koneksi/koneksi.php';
?>


<?php
    // include_once '../../koneksi/koneksi.php';
    $koneksi      = mysqli_connect("localhost", "root", "", "biodata_teman_ti");
    $matkul_nama  = mysqli_query($koneksi,"SELECT `data_diri_matkul`.`id_matkul`,COUNT(*), `matkul`.`nama_matkul` from `data_diri_matkul`, `matkul` where `matkul`.`id_matkul` = `data_diri_matkul`.`id_matkul` GROUP BY `matkul`.`id_matkul` ");
     $matkul_count= mysqli_query($koneksi,"SELECT `data_diri_matkul`.`id_matkul`,COUNT(*), `matkul`.`nama_matkul` from `data_diri_matkul`, `matkul` where `matkul`.`id_matkul` = `data_diri_matkul`.`id_matkul` GROUP BY `matkul`.`id_matkul` ");

    $komunitas_nama    = mysqli_query($koneksi,"SELECT komunitas.`id_komunitas`,COUNT(*), `komunitas`.`nama_komunitas` from `data_diri_komunitas`, `komunitas` where `komunitas`.`id_komunitas` = `data_diri_komunitas`.`id_komunitas` GROUP BY `komunitas`.`id_komunitas`");
    $komunitas_count    = mysqli_query($koneksi,"SELECT komunitas.`id_komunitas`,COUNT(*), `komunitas`.`nama_komunitas` from `data_diri_komunitas`, `komunitas` where `komunitas`.`id_komunitas` = `data_diri_komunitas`.`id_komunitas` GROUP BY `komunitas`.`id_komunitas`");

    $skill_nama        = mysqli_query($koneksi,"SELECT `data_diri_skill`.`id_skill`,COUNT(*), `skill`.`nama_skill` from `data_diri_skill`, `skill` where `skill`.`id_skill` = `data_diri_skill`.`id_skill` GROUP BY `skill`.`id_skill` ");
     $skill_count        = mysqli_query($koneksi,"SELECT `data_diri_skill`.`id_skill`,COUNT(*), `skill`.`nama_skill` from `data_diri_skill`, `skill` where `skill`.`id_skill` = `data_diri_skill`.`id_skill` GROUP BY `skill`.`id_skill` ");
?>


      <!-- page content -->

      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <!-- <h3>Chart</h3> -->
            </div>
          </div>
          <div class="clearfix"></div>
        </div>






      <!-- ======================Tabel Data=========================== -->




          <div class="row" id="modal_form">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_content">
               <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Matakuliah</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Komunitas</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Skill</a>
                      </li>
                    </ul>

                    <div id="myTabContent" class="tab-content">
                      <!-- ===========================Isi Tab Matkul======================== -->
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <div class="container" style=" width: 80%; margin: 15px auto;">
                              <canvas id="chart_matkul" width="500" height="300"></canvas>
                            </div>
                      </div>
                      <!-- =======================Isi Tab Matkul========================= -->

                      <!-- ========================Isi Tab Komunitas======================= -->
                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <div class="container" style=" width: 80%; margin: 15px auto;">
                              <canvas id="chart_komunitas" width="500" height="300"></canvas>
                            </div>
                          <!-- ======================Isi Tab Skill======================= -->

                      </div>


                      <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <div class="container" style=" width: 80%; margin: 15px auto;">
                              <canvas id="chart_skill" width="500" height="300"></canvas>
                            </div>
                      </div>
                    </div>
                  </div>


              </div>
            </div>
          </div>
        </div>


      <!-- ======================Tabel Data=========================== -->




        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right"> Praktikum WEB 2017  - Asifatul Mu'awanah & Dinda Ockta N
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->



<?php
      include('../footer.php');
?>

























<!-- ==============Chart Maktkul==================== -->
<script>
    // var coba=2;
    var ctx = document.getElementById("chart_matkul");
    var chart_matkul = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                      <?php
                          // $coba=2;
                          while ($b = mysqli_fetch_array($matkul_nama)) {
                                 // if($b['COUNT(*)']==$coba){
                                     echo '"' . $b['nama_matkul'] . '",';
                                  // }
                          }
                      ?>
                    ],
            datasets: [{
                    label: '# of Votes',
                    data: [
                            <?php
                                // $coba=2;
                                while ($p = mysqli_fetch_array($matkul_count)) {
                                  // if($p['COUNT(*)']==$coba){
                                    echo '"' . $p['COUNT(*)'] . '",';
                                  // }
                                }
                            ?>
                          ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderWidth: 1
                }]
        },

        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });
</script>
<!-- ==============Chart Maktkul==================== -->







<!-- ==============Chart Komunitas==================== -->
<script>
    var ctx = document.getElementById("chart_komunitas");
    var chart_komunitas = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php while ($b = mysqli_fetch_array($komunitas_nama)) {
                                 echo '"' . $b['nama_komunitas'] . '",';
                             }?>],
            datasets: [{
                    label: '# of Votes',
                    data: [<?php while ($p = mysqli_fetch_array($komunitas_count)) { echo '"' . $p['COUNT(*)'] . '",';}?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderWidth: 1
                }]
        },

        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });
</script>
<!-- ==============Chart Komunitas==================== -->









<!-- ==============Chart Skill==================== -->
<script>
    var ctx = document.getElementById("chart_skill");
    var chart_skill = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php while ($b = mysqli_fetch_array($skill_nama)) {
                                 echo '"' . $b['nama_skill'] . '",';
                             }?>],
            datasets: [{
                    label: '# of Votes',
                    data: [<?php while ($p = mysqli_fetch_array($skill_count)) { echo '"' . $p['COUNT(*)'] . '",';}?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderWidth: 1
                }]
        },

        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });
</script>
<!-- ==============Chart Skill==================== -->
