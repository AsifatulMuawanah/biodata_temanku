<?php
    // include_once '../../koneksi/koneksi.php';
    $koneksi     = mysqli_connect("localhost", "root", "", "biodata_teman_ti");
    $angkatan = mysqli_query($koneksi,"SELECT `tahun_angkatan` FROM `angkatan` ORDER BY `angkatan`.`tahun_angkatan` ASC ");
     $angkatan1 = mysqli_query($koneksi,"SELECT `id_angkatan` FROM `angkatan` ORDER BY `angkatan`.`tahun_angkatan` ASC ");
?>

<!doctype html>
<html>

<head>
    <title>Bar Chart Multi Axis</title>
    <script src="../../assets/gentelella/production/js/jquery.min.js"></script>
    <script src="../../assets/Chart/Chart.bundle.js"></script>
    <style type="text/css">
            .container {
                width: 40%;
                margin: 15px auto;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <canvas id="myChart" width="100" height="100"></canvas>
        </div>
        <script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [<?php while ($b = mysqli_fetch_array($angkatan)) {
                                         echo '"' . $b['tahun_angkatan'] . '",';
                                     }?>],
                    datasets: [{
                            label: '# of Votes',
                            data: [<?php while ($p = mysqli_fetch_array($angkatan1)) { echo '"' . $p['id_angkatan'] . '",';}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>





       
    </body>

</html>
