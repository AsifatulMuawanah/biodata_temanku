<?php
  include '../../koneksi/koneksi.php';
  $m="SELECT `data_diri_matkul`.`id_matkul`,COUNT(*), `matkul`.`nama_matkul` from `data_diri_matkul`, `matkul` where `matkul`.`id_matkul` = `data_diri_matkul`.`id_matkul` GROUP BY `matkul`.`id_matkul`";
  $query_matkul = mysql_query($m);
  $k="SELECT komunitas.`id_komunitas`,COUNT(*), `komunitas`.`nama_komunitas` from `data_diri_komunitas`, `komunitas` where `komunitas`.`id_komunitas` = `data_diri_komunitas`.`id_komunitas` GROUP BY `komunitas`.`id_komunitas`";
  $query_komunitas    = mysql_query($k);
  $s="SELECT `data_diri_skill`.`id_skill`,COUNT(*), `skill`.`nama_skill` from `data_diri_skill`, `skill` where `skill`.`id_skill` = `data_diri_skill`.`id_skill` GROUP BY `skill`.`id_skill` ";
  $query_skill        = mysql_query($s);
?>

<!DOCTYPE html>
<html>
<head>
  <title>coba</title>
    <link rel="stylesheet" type="text/css" href="c.css">
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
</head>
<body>
  <div id="charts">
  <div id="chartdiv1"></div>
  <div id="chartdiv2"></div>
</div>
<div id="filter">
  <label style="color: #67b7dc;"><input type="checkbox" value="Executive" checked="checked" onclick="applyFilters()" class="filter-position" /> Executive/Matkul</label><br />
  <label style="color: #fdd400;"><input type="checkbox" value="Research" checked="checked" onclick="applyFilters()" class="filter-position" /> Research/Komunitas</label><br />
<!--   <label style="color: #84b761;"><input type="checkbox" value="Marketing" checked="checked" onclick="applyFilters()" class="filter-position" /> Marketing</label><br /> -->
  <label style="color: #cc4748;"><input type="checkbox" value="Sales" checked="checked" onclick="applyFilters()" class="filter-position" /> Sales/Skill</label><br />

  <label>
    Name:
    <input id="filter-name" type="text" onchange="applyFilters()" />
  </label>
</div>
</body>
</html>


<!-- <script type="text/javascript" src="j.js"></script> -->



<script type="text/javascript">
   $(document).ready(function() {
        var chart1 = AmCharts.makeChart( "chartdiv1", {
  "type": "serial",
  "dataProvider": getFilteredData(),
  "graphs": [ {
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "lineColorField": "color",
    "fillColorsField": "color",
    "type": "column",
    "valueField": "salary",
    "balloonText": "[[category]] ([[position]])<br><span style='font-size: 150%;'>$[[value]]</span>"
  } ],
  "categoryField": "employee",
  "valueAxes": [{
    "title": "Annual salary (US$)"
  }]
} );


var chart2 = AmCharts.makeChart( "chartdiv2", {
  "type": "serial",
  "dataProvider": getFilteredData(),
  "graphs": [ {
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "lineColorField": "color",
    "fillColorsField": "color",
    "type": "column",
    "valueField": "salary",
    "balloonText": "[[category]] ([[position]])<br><span style='font-size: 150%;'>$[[value]]</span>"
  } ],
  "categoryField": "employee",
  "valueAxes": [{
    "title": "Annual salary (US$)"
  }]
} );
      });
</script>



<script type="text/javascript">
  var sourceData = [ 
        <?php
          while($data_matkul=mysql_fetch_array($query_matkul)){
            echo '{';
            echo '"employee":"'.$data_matkul['nama_matkul'].'",';
            echo '"salary:"'.$data_matkul['COUNT(*)'].',';
            echo '"position":"Executive"';
            echo '},';
          }
        ?>
        <?php
          while($data_komunitas=mysql_fetch_array($query_komunitas)){
            echo '{';
            echo '"employee":"'.$data_komunitas['nama_komunitas'].'",';
            echo '"salary:"'.$data_komunitas['COUNT(*)'].',';
            echo '"position":"Research"';
            echo '},';
          }
        ?>
        <?php
          while($data_skill=mysql_fetch_array($query_skill)){
            echo '{';
            echo '"employee":"'.$data_skill['nama_skill'].'",';
            echo '"salary:"'.$data_skill['COUNT(*)'].',';
            echo '"position":"Sales"';
            echo '},';
          }
        ?>
  ];



/**
 * Set up colors for each of the position
 */
var positionColors = {
  "Executive": "#67b7dc",
  "Research": "#fdd400",
  // "Marketing": "#84b761",
  "Sales": "#cc4748"
};

function lowercase( string ) {
  return string.toLocaleLowerCase();
}

function contains( string, value ) {
  return string.indexOf( value ) !== -1;
}

/**
 * Function that checks which data is selected and generates a new data set
 */
function getFilteredData() {
  var name = lowercase( document.getElementById( "filter-name" ).value );

  var filters = {};

  // get all filter checkboxes
  var fields = document.getElementsByClassName( "filter-position" );
  for ( var i = 0; i < fields.length; i++ ) {
    if ( fields[ i ].checked ) {
      filters[ fields[ i ].value ] = true;
    }
  }

  // init new data set
  var newData = [];

  // cycle through source data and filter out required data points
  for ( var i = 0; i < sourceData.length; i++ ) {
    var dataPoint = sourceData[ i ];

    if ( filters[ dataPoint.position ] &&
         contains( lowercase( dataPoint.employee ), name ) ) {
      newData.push( {
        "employee": dataPoint.employee,
        "salary": dataPoint.salary,
        "position": dataPoint.position,
        "color": positionColors[ dataPoint.position ]
      } );
    }
  }

  // return new data set
  return newData;

}

/**
 * Function which applies current filters when invoked
 */
function applyFilters() {
  var data = getFilteredData();

  // update chart data
  chart1.dataProvider = data;
  chart2.dataProvider = data;
  chart1.validateData();
  chart2.validateData();
}

/**
 * Create chart
 */
var chart1 = AmCharts.makeChart( "chartdiv1", {
  "type": "serial",
  "dataProvider": getFilteredData(),
  "graphs": [ {
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "lineColorField": "color",
    "fillColorsField": "color",
    "type": "column",
    "valueField": "salary",
    "balloonText": "[[category]] ([[position]])<br><span style='font-size: 150%;'>$[[value]]</span>"
  } ],
  "categoryField": "employee",
  "valueAxes": [{
    "title": "Annual salary (US$)"
  }]
} );


var chart2 = AmCharts.makeChart( "chartdiv2", {
  "type": "serial",
  "dataProvider": getFilteredData(),
  "graphs": [ {
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "lineColorField": "color",
    "fillColorsField": "color",
    "type": "column",
    "valueField": "salary",
    "balloonText": "[[category]] ([[position]])<br><span style='font-size: 150%;'>$[[value]]</span>"
  } ],
  "categoryField": "employee",
  "valueAxes": [{
    "title": "Annual salary (US$)"
  }]
} );
</script>