            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Skill</th>
                    <th>Aksi</th>
                  </tr>
                </thead>

                <tbody >
                  <?php
                    include_once '../../koneksi/koneksi.php';
                    $query="select * from skill ORDER BY `skill`.`nama_skill` ASC";
                    $eksekusi=mysql_query($query);
                    $no=1;
                    while($data=mysql_fetch_array($eksekusi))
                    { ?>
                      <tr>
                      <td>
                        <?php echo $no; ?>
                      </td>
                      <td>
                        <?php echo $data['nama_skill']; ?>
                      </td>
                      
                      <td>                     
                          <button type="button" class="btn btn-round bg-green"  onclick="edit(<?=$data['id_skill'];?>)">Edit</button>

                          <button type="button" class="btn btn-round btn-warning"  onclick="hapus(<?=$data['id_skill'];?>)">Hapus</button>
                      </td>
                    </tr>
                    <?php
                      $no++;}
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                    <th>No</th>
                    <th>Skill</th>
                    <th>Aksi</th>
                    </tr>
                </tfoot>
              </table>

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({
      keys: true
    });
    $('#datatable-responsive').DataTable();
    $('#datatable-scroller').DataTable({
      ajax: "../../assets/gentelella/production/js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });
    var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });
  });
  TableManageButtons.init();
</script>
    


