     // ======================== Menampilkan Data di Database =============================
     // ======================== Menampilkan Data di Database =============================


      function tabel_data(){
         $.get("data_matkul.php", {}, function (data, status) {
            $(".x_content").html(data);
        });
      }

    // ======================== Menampilkan Data di Database ============================= -->






    // ======================== Action Tombol Tambah =============================

       function tambah()
       {
          // save_method = 'add';
          $('#demo-form2')[0].reset(); 
          $('#form1').modal('show'); 
          $('.modal-title').text('Tambah');

          button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="simpan()">'+
                    'tambah'+
                  '</button>';
          $(".modal-footer").html(button);
        }

    // ======================== Action Tombol Tambah =============================

    // ======================== Simpan Data ============================= -->
        function simpan() {
          $.ajax({
            url: "aksi.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=simpan",
            success: function(res) {
              $('#form1').modal('hide');
              tabel_data();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Simpan Data ============================= -->





    // ======================== Action TOmbol Edit=============================

       function edit(id_matkul)
       {
          $('#form1').modal('show'); 
          $('.modal-title').text('Perbarui');
            button='<button type="button" class="btn btn-default" data-dismiss="modal">'+
                    'Close'+
                 '</button>'+
                 '<button type="button" class="btn bg-green" onclick="perbarui()">'+
                    'perbarui'+
                  '</button>';

          $.ajax({
          url: "aksi.php",
          type: "post",
          data: "proses=edit&id="+id_matkul,
          dataType: "json",
          success: function(res) {
            $('[name="id_matkul"]').val(res.id_matkul);
            $('[name="nama_matkul"]').val(res.nama_matkul);
            $(".modal-footer").html(button);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });

        }

    // ======================== Action TOmbol Edit=============================





   
    // ======================== Perbarui Data ============================= -->
        function perbarui() {
          $.ajax({
            url: "aksi.php",
            type: "post",
            data: $('#demo-form2').serialize()+"&proses=perbarui",
            success: function(res) {
              $('#form1').modal('hide');
              tabel_data();

            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
          });
        }
    // ======================== Perbarui Data ============================= -->










    // ======================== Hapus Data ============================= -->
         function hapus(id_matkul) {
          var conf = confirm("Apakah ingin di hapus?");
          if (conf == true) {
              $.ajax({
                url: "aksi.php",
                type: "post",
                data: "proses=hapus&id="+id_matkul,
                success: function(res) {
                  tabel_data();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
                }
              });
          }
        }
    // ======================== Hapus Data ============================= -->
