<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Biodata Temanku </title>

  <!-- Bootstrap core CSS -->

  <link href="assets/gentelella/production/css/bootstrap.min.css" rel="stylesheet">

  <link href="assets/gentelella/production/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/gentelella/production/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="assets/gentelella/production/css/custom.css" rel="stylesheet">
  <link href="assets/gentelella/production/css/icheck/flat/green.css" rel="stylesheet">


  <script src="assets/gentelella/production/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
          <form id="masuk">
            <h1>Masuk</h1>
            <div>
              <input type="text" class="form-control" placeholder="NIM atau Email" required=""  name="nim" />
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" required="" name="password" />
            </div>
            <div>
              <a class="btn btn-default submit" onclick="masuk()">Masuk</a>
              <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Belum Punya Akun?
                <a href="#toregister" class="to_register"> Buat Akun</a>
              </p>
              <div class="clearfix"></div>
              <br />
              <div>
                <h1><i class="fa fa-users" style="font-size: 26px;"></i> Biodata Teman</h1>
                <p>©2017 Praktikum Website Kelas H</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>


      <div id="register" class="animate form">
        <section class="login_content">
          <form id="daftar">
            <h1>Buat Akun</h1>
            <div>
              <input type="text" class="form-control" placeholder="Nim" required="" name="nim" />
            </div>
            <div>
              <input type="text" class="form-control" placeholder="Nama" required="" name="nama" />
            </div>
            <div>
              <input type="email" class="form-control" placeholder="Email" required="" name="email" />
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" required="" name="password" />
            </div>
            <div class="form-group">
              <label>Apakah anda Ketua/Wakil Ketua angkatan?</label><br>
              
              <!-- <div class="col-md-6 col-sm-6 col-xs-12"> -->
                <div id="ketua" class="btn-group" data-toggle="buttons">
                  <label>
                    <input type="radio" name="ketua_angkatan" value="Iya"> &nbsp; Iya &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="ketua_angkatan" value="Tidak"> Tidak
                  </label>
                </div>
              <!-- </div> -->
              <br><br>

            <div>
              <a class="btn btn-default submit" onclick="daftar()">daftar</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Sudah Punya Akun ?
                <a href="#tologin" class="to_register"> Masuk </a>
              </p>
              <div class="clearfix"></div>
              <br />
               <div>
                <h1><i class="fa fa-users" style="font-size: 26px;"></i> Biodata Teman</h1>
                <p>©2017 Praktikum Website Kelas H</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>

</body>

</html>












<script type="text/javascript" src="index/script_index.js"> </script>